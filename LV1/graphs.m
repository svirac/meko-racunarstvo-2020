close all;
clear;
clc;

% Set filenames
path = 'csv-ovi/cjelobrojni';
f1name = 'qs24-p50-m0.04-e4-5.csv';
f2name = 'qs24-p50-m0.04-e8-3.csv';
f3name = 'qs24-p50-m0.04-e16-2.csv';

f1 = fullfile(path, f1name);
f2 = fullfile(path, f2name);
f3 = fullfile(path, f3name);


% Read .csv files
dataTable1 = readtable(f1);
dataTable2 = readtable(f2);
dataTable3 = readtable(f3);

data1 = table2array(dataTable1);
data2 = table2array(dataTable2);
data3 = table2array(dataTable3);


% generate arrays from .csv files
gens1 = data1(:, 1);
fit1 = data1(:, 2);

gens2 = data2(:, 1);
fit2 = data2(:, 2);

gens3 = data3(:, 1);
fit3 = data3(:, 2);

% plot graph
figure
plot(gens1, fit1, 'r');
hold on;
plot(gens2, fit2, 'g');
hold on;
plot(gens3, fit3, 'b');
title('Ovisnost o broju elitnih clanova za 24x24');
xlabel('Broj generacija');
ylabel('Vrijednost fitness funkcije');
legend({'4', '8', '16'});
