close all;
clear;
clc;

% Set filenames
path = 'csv-ovi/';
f1name = '../func_75.csv';
f2name = '0.csv';
f3name = '1.csv';
f4name = '2.csv';
%f5name = '3.csv';

f1 = fullfile(path, f1name);
f2 = fullfile(path, f2name);
f3 = fullfile(path, f3name);
f4 = fullfile(path, f4name);
%f5 = fullfile(path, f5name);

% Read .csv files
dataTable1 = readtable(f1);
dataTable2 = readtable(f2);
dataTable3 = readtable(f3);
dataTable4 = readtable(f4);
%dataTable5 = readtable(f5);

data1 = table2array(dataTable1);
data2 = table2array(dataTable2);
data3 = table2array(dataTable3);
data4 = table2array(dataTable4);
%data5 = table2array(dataTable5);

% generate arrays from .csv files
x = data1(:, 1);
y = data1(:, 2);

% plot graph
figure
plot(x, y, 'g', 'LineWidth', 2);
hold on;

plot(x, data2, '--r', 'LineWidth', 1);
hold on;

% plot(x, data3, 'b', 'LineWidth', 1);
% hold on;
% 
% plot(x, data4, 'm', 'LineWidth', 1);
% hold on;

% plot(x, data5, 'c', 'LineWidth', 1);
% hold on;

grid on;
title('Estimacija funkcije za optimalne vrijednosti parametara');

legend({'Prava funkcija', 'Estimirana funkcija'}, 'Location','southwest');
