import numpy as np
import matplotlib.pyplot as plt
import skfuzzy as fuzz
from skfuzzy import control as ctrl


# Ulazna varijabla
greska = ctrl.Antecedent(np.linspace(0, 30, 100), 'greska')
# Izlazna varijabla
grijac = ctrl.Consequent(np.linspace(0, 10, 100), 'grijac')

# Funkcije pripadnosti
mf = 'gauss'

if mf == 'trokut':
    greska['niska'] = fuzz.trimf(greska.universe, [0, 0, 15])
    greska['srednja'] = fuzz.trimf(greska.universe, [0, 15, 30])
    greska['visoka'] = fuzz.trimf(greska.universe, [15, 30, 30])

    grijac['slabo'] = fuzz.trimf(grijac.universe, [0, 0, 5])
    grijac['srednje'] = fuzz.trimf(grijac.universe, [0, 5, 10])
    grijac['jako'] = fuzz.trimf(grijac.universe, [5, 10, 10])

elif mf == 'trapez':
    greska['niska'] = fuzz.trapmf(greska.universe, [0, 0, 5, 10])
    greska['srednja'] = fuzz.trapmf(greska.universe, [5, 10, 20, 25])
    greska['visoka'] = fuzz.trapmf(greska.universe, [20, 25, 30, 30])

    grijac['slabo'] = fuzz.trapmf(grijac.universe, [0, 0, 2, 4])
    grijac['srednje'] = fuzz.trapmf(grijac.universe, [0, 4, 6, 10])
    grijac['jako'] = fuzz.trapmf(grijac.universe, [6, 8, 10, 10])

else:
    greska['niska'] = fuzz.gaussmf(greska.universe, 0, 5)
    greska['srednja'] = fuzz.gaussmf(greska.universe, 15, 5)
    greska['visoka'] = fuzz.gaussmf(greska.universe, 30, 5)

    grijac['slabo'] = fuzz.gaussmf(grijac.universe, 0, 2)
    grijac['srednje'] = fuzz.gaussmf(grijac.universe, 5, 2)
    grijac['jako'] = fuzz.gaussmf(grijac.universe, 10, 2)

# Prikaz funkcija pripadnosti
greska.view()
grijac.view()
plt.show()

# Pravila
rule1 = ctrl.Rule(greska['niska'], grijac['slabo'])
rule2 = ctrl.Rule(greska['srednja'], grijac['srednje'])
rule3 = ctrl.Rule(greska['visoka'], grijac['jako'])

# Regulacijski sustav
reg = ctrl.ControlSystem([rule1, rule2, rule3])

# Simulacija
reg_sim = ctrl.ControlSystemSimulation(reg)

# Ispitivanje regulatora na 100 točaka
interval = np.linspace(0, 30, 100)
output = []
for i in interval:
    reg_sim.input['greska'] = i
    reg_sim.compute()
    output.append(reg_sim.output['grijac'])

# Prikaz prijenosne funkcije
plt.plot(interval, output)
plt.show()