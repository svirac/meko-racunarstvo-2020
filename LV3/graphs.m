close all;
clear;
clc;

% Set filenames
path = 'csv-dim10/pso';
f1name = 'i0.0-p0.5-s0.5-2.csv';
f2name = 'i0.37-p0.5-s0.5-4.csv';
f3name = 'i0.74-p0.5-s0.5-4.csv';

f1 = fullfile(path, f1name);
f2 = fullfile(path, f2name);
f3 = fullfile(path, f3name);

% Read .csv files
dataTable1 = readtable(f1);
dataTable2 = readtable(f2);
dataTable3 = readtable(f3);

data1 = table2array(dataTable1);
data2 = table2array(dataTable2);
data3 = table2array(dataTable3);

% generate arrays from .csv files
gens1 = data1(:, 1);
fit1 = data1(:, 2);

gens2 = data2(:, 1);
fit2 = data2(:, 2);

gens3 = data3(:, 1);
fit3 = data3(:, 2);

% plot graph
figure
plot(gens1, fit1, '-r');
hold on;
plot(gens2, fit2, '--g');
hold on;
plot(gens3, fit3, '-.b');
hold on;
grid on;
title('Ovisnost o mjeri inercije');
xlabel('Broj generacija');
ylabel('Vrijednost fitness funkcije');
legend({'0.0', '3.7', '7.4'});
