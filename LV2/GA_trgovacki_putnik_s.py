# Path intersect with border algorithm adapted from: https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
# -*- coding: utf-8 -*-

import sys
import os
import random
import math
import xml.etree.ElementTree as ET
import numpy as np

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPainter, QBrush, QPen, QPixmap, QImage, QFont
from PyQt5.QtCore import Qt, QRect
from PyQt5.QtChart import QLineSeries, QChart, QValueAxis, QChartView
from PyQt5.QtWidgets import QFileDialog


from deap import base, creator, tools

#Change current working directory to this script path
import pathlib
os.chdir(pathlib.Path(__file__).parent.absolute())

####Global GA parameters####
IND_SIZE = 0 #Size of the individual (number of cities)
NGEN = 5000 #number of generations
POP_SIZE = 100  #population size
MUTPB = 0.02 #probability for mutating an individual
NELT = 4    #number of elite individuals
#########################

####Other global variables####
stop_evolution = False
q_min_series = QLineSeries()
q_min_series.setName("MIN")
q_max_series = QLineSeries()
q_max_series.setName("MAX")
q_avg_series = QLineSeries()
q_avg_series.setName("AVG")
croatia_map_img = QImage("Croatia620.png")
gradovi = []
sirine = []
duzine = []
gr_sirine = []
gr_duzine = []
border_check = False
solutions = []
##############################

#Load the list of cities
tree = ET.parse('gradovi.xml')
root = tree.getroot()
for child in root:
    gradovi.append(str(child.attrib['ime_grada']))
    sirine.append(float(child.attrib['sirina']))
    duzine.append(float(child.attrib['duzina']))

# Load the list of borders
tree = ET.parse('border.xml')
root = tree.getroot()
for child in root:
    gr_sirine.append(float(child.attrib['sirina']))
    gr_duzine.append(float(child.attrib['duzina']))


#Set the number of cities when they have been parsed
IND_SIZE=len(gradovi)

# Matrix which contains paths that cross the border
borderCross_paths = np.zeros((IND_SIZE, IND_SIZE))


# Fitness function
def evaluateInd(individual):
    fit_val = 0.0
    distance = 0

    for i in range(len(individual)-1):           
        sirina = (sirine[individual[i]] - sirine[individual[i+1]])
        sirina *= 110.64
        duzina = (duzine[individual[i]] - duzine[individual[i+1]]) 
        duzina *=  78.85
        distance = math.sqrt(sirina**2 + duzina**2)
        fit_val += distance

        # Check if path crosses the border
        if border_check:
            if borderCross_paths[individual[i]][individual[i+1]] == 1:
                fit_val += 1000

    return fit_val,#returning must be a tuple becos of posibility of optimization via multiple goal values (objectives)

def GlobToImgCoords(coord_x, coord_y):
    stupnjevi_1 = math.floor(coord_x)
    minute_1 = round((coord_x - math.floor(coord_x)) * 60)
    stupnjevi_2 = math.floor(coord_y)
    minute_2 = round((coord_y - math.floor(coord_y)) * 60)
    
    kor_x = 0
    kor_y = 0
    if stupnjevi_2 > 13:
        kor_x = ((stupnjevi_2 - (14)) * 60) + (minute_2 + 54)
    else:
        kor_x = minute_2 - 6

    if stupnjevi_1 < 46:
        kor_y = (((46 - (stupnjevi_1 + 1)) * 60) + (48 + (60 - minute_1)))
    else:
        kor_y = (48 - minute_1)

    kor_x = kor_x + math.floor(kor_x * 0.52)
    kor_y = (kor_y * 2) + math.floor(kor_y * 0.12)

    return kor_x, kor_y


def generateWorldImage(individual):
    #Create a transparent image
    img = QImage(620, 600, QImage.Format_ARGB32)
    img.fill(Qt.transparent)
    
    #Create a painter
    painter = QPainter(img)
    
    #Highlight first and last town
    g_first = individual[0]
    g_last = individual[IND_SIZE - 1]
    x1, y1 = GlobToImgCoords(sirine[g_first], duzine[g_first])
    x2, y2 = GlobToImgCoords(sirine[g_last], duzine[g_last])
    painter.setBrush(Qt.black)
    a = QtCore.QPoint(x1-8, y1-8)
    b = QtCore.QPoint(x1-8, y1+8)
    c = QtCore.QPoint(x1+8, y1)
    points = QtGui.QPolygon([a, b, c])
    painter.drawPolygon(points)
    painter.drawEllipse(x2-7, y2-7, 14, 14)
    
    #Drawing Path
    painter.setPen(QPen(Qt.black,  2, Qt.DashLine))
    for i in range(IND_SIZE - 1): #
        x1, y1 = GlobToImgCoords(sirine[individual[i]], duzine[individual[i]])
        x2, y2 = GlobToImgCoords(sirine[individual[i + 1]], duzine[individual[i + 1]])
        painter.drawLine(x1, y1, x2, y2)

        ## Paint border
        # painter.setPen(QPen(Qt.red,  3, Qt.DashLine))
        # for i in range(len(gr_duzine) - 1): #
        #     x1, y1 = GlobToImgCoords(gr_sirine[i], gr_duzine[i])
        #     x2, y2 = GlobToImgCoords(gr_sirine[i+1], gr_duzine[i+1])
        #     painter.drawLine(x1, y1, x2, y2)
    
    #Finish painter
    painter.end()
    
    #Return finished image
    return img
        

class MyQFrame(QtWidgets.QFrame):
    def paintEvent(self, event):
        painterWorld = QPainter(self)
        painterWorld.drawPixmap(self.rect(), self.img)
        painterWorld.end()

class Ui_MainWindow(QtWidgets.QMainWindow):
    def setupUi(self):
        self.setObjectName("MainWindow")
        self.resize(850, 1080)
        self.setWindowTitle("GA - Queens")
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.frameWorld = MyQFrame(self.centralwidget)
        self.frameWorld.img = QPixmap(1000,1000)
        self.frameWorld.setGeometry(QtCore.QRect(10, 10, 620, 600))
        self.frameWorld.setFrameShape(QtWidgets.QFrame.Box)
        self.frameWorld.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frameWorld.setObjectName("frameWorld")
        self.frameChart = QChartView(self.centralwidget)
        self.frameChart.setGeometry(QtCore.QRect(10, 620, 620, 400))
        self.frameChart.setFrameShape(QtWidgets.QFrame.Box)
        self.frameChart.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frameChart.setRenderHint(QPainter.Antialiasing)
        self.frameChart.setObjectName("frameChart")
        self.gaParams = QtWidgets.QGroupBox(self.centralwidget)
        self.gaParams.setGeometry(QtCore.QRect(650, 10, 161, 145))
        self.gaParams.setObjectName("gaParams")
        self.gaParams.setTitle("GA parameters")
        self.label1 = QtWidgets.QLabel(self.gaParams)
        self.label1.setGeometry(QtCore.QRect(10, 20, 61, 16))
        self.label1.setObjectName("label1")
        self.label1.setText("Population:")
        self.label2 = QtWidgets.QLabel(self.gaParams)
        self.label2.setGeometry(QtCore.QRect(10, 50, 47, 16))
        self.label2.setObjectName("label2")
        self.label2.setText("Mutation:")
        self.label3 = QtWidgets.QLabel(self.gaParams)
        self.label3.setGeometry(QtCore.QRect(10, 80, 81, 16))
        self.label3.setObjectName("label3")
        self.label3.setText("Elite members:")
        self.label4 = QtWidgets.QLabel(self.gaParams)
        self.label4.setGeometry(QtCore.QRect(10, 110, 91, 16))
        self.label4.setObjectName("label4")
        self.label4.setText("No. generations:")
        self.tbxPopulation = QtWidgets.QLineEdit(self.gaParams)
        self.tbxPopulation.setGeometry(QtCore.QRect(100, 20, 51, 20))
        self.tbxPopulation.setObjectName("tbxPopulation")
        self.tbxMutation = QtWidgets.QLineEdit(self.gaParams)
        self.tbxMutation.setGeometry(QtCore.QRect(100, 50, 51, 20))
        self.tbxMutation.setObjectName("tbxMutation")
        self.tbxElite = QtWidgets.QLineEdit(self.gaParams)
        self.tbxElite.setGeometry(QtCore.QRect(100, 80, 51, 20))
        self.tbxElite.setObjectName("tbxElite")
        self.tbxGenerations = QtWidgets.QLineEdit(self.gaParams)
        self.tbxGenerations.setGeometry(QtCore.QRect(100, 110, 51, 20))
        self.tbxGenerations.setObjectName("tbxGenerations")
        self.cbxNoVis = QtWidgets.QCheckBox(self.centralwidget)
        self.cbxNoVis.setGeometry(QtCore.QRect(650, 170, 170, 17))
        self.cbxNoVis.setObjectName("cbxNoVis")
        self.cbxNoVis.setText("No visualization per generation")
        self.cbxBorder = QtWidgets.QCheckBox(self.centralwidget)
        self.cbxBorder.setGeometry(QtCore.QRect(650, 200, 100, 17))
        self.cbxBorder.setObjectName("cbxBorder")
        self.cbxBorder.setText("Border patrol")
        self.btnStart = QtWidgets.QPushButton(self.centralwidget)
        self.btnStart.setGeometry(QtCore.QRect(650, 230, 75, 23))
        self.btnStart.setObjectName("btnStart")
        self.btnStart.setText("Start")
        self.btnStop = QtWidgets.QPushButton(self.centralwidget)
        self.btnStop.setEnabled(False)
        self.btnStop.setGeometry(QtCore.QRect(730, 230, 75, 23))
        self.btnStop.setObjectName("btnStop")
        self.btnStop.setText("Stop")
        self.btnIterate = QtWidgets.QPushButton(self.centralwidget)
        self.btnIterate.setGeometry(QtCore.QRect(690, 310, 100, 30))
        self.btnIterate.setObjectName("btnIterate")
        self.btnIterate.setText("Iterate")
        self.btnSaveWorld = QtWidgets.QPushButton(self.centralwidget)
        self.btnSaveWorld.setGeometry(QtCore.QRect(650, 570, 121, 41))
        self.btnSaveWorld.setObjectName("btnSaveWorld")
        self.btnSaveWorld.setText("Save world as image")
        self.btnSaveChart = QtWidgets.QPushButton(self.centralwidget)
        self.btnSaveChart.setGeometry(QtCore.QRect(650, 930, 121, 41))
        self.btnSaveChart.setObjectName("btnSaveChart")
        self.btnSaveChart.setText("Save chart as image")
        self.btnSaveChartSeries = QtWidgets.QPushButton(self.centralwidget)
        self.btnSaveChartSeries.setGeometry(QtCore.QRect(650, 980, 121, 41))
        self.btnSaveChartSeries.setObjectName("btnSaveChartSeries")
        self.btnSaveChartSeries.setText("Save chart as series")
        self.setCentralWidget(self.centralwidget)
        QtCore.QMetaObject.connectSlotsByName(self)
        
        #Connect events
        self.btnStart.clicked.connect(self.btnStart_Click)
        self.btnStop.clicked.connect(self.btnStop_Click)
        self.btnIterate.clicked.connect(self.btnIterate_Click)
        self.btnSaveWorld.clicked.connect(self.btnSaveWorld_Click)
        self.btnSaveChart.clicked.connect(self.btnSaveChart_CLick)
        self.btnSaveChartSeries.clicked.connect(self.btnSaveChartSeries_Click)
        
        #Set default GA variables
        self.tbxGenerations.insert(str(NGEN))
        self.tbxPopulation.insert(str(POP_SIZE))
        self.tbxMutation.insert(str(MUTPB))
        self.tbxElite.insert(str(NELT))
        
        self.new_image = QPixmap(1000,1000)


    # Iterate through combinations of population, mutation and elitism
    def btnIterate_Click(self):
        self.btnIterate.setEnabled(False)
        global POP_SIZE 
        global MUTPB 
        global NELT
        POP_SIZE = 50
        MUTPB = 0.1
        NELT = 10

        print("1. Population size:")
        # Go through populations
        for j in range(4):
            print("     ",  POP_SIZE, end=" ==> ")
            for i in range(1, 6):
                self.btnStart_Click(i)
            POP_SIZE *= 2
        POP_SIZE = 100

        print("2. Mutation percent:")
        # Go through mutations
        MUTPB = 0.05
        for j in range(4):
            if MUTPB == 0.1:
                MUTPB = round(MUTPB + 0.05, 2)
                continue
            print("     ",  round(MUTPB*100), end="%  ==> ")
            for i in range(1, 6):
                self.btnStart_Click(i)
            MUTPB = round(MUTPB + 0.05, 2)
        MUTPB = 0.1

        print("3. Number of elite individuals:")
        # Go through elitism
        NELT = 5
        for j in range(4):
            if NELT == 10: 
                NELT += 5
                continue
            print("     ",  NELT, end=" ==> ")
            for i in range(1, 6): 
                self.btnStart_Click(i)
            NELT += 5
        NELT = 10

        self.btnIterate.setEnabled(True)

        
    def btnStart_Click(self, iteration):
        #Set global variables
        global stop_evolution
        global q_min_series
        global q_max_series
        global q_avg_series
        stop_evolution = False    
        q_min_series.clear()      
        q_max_series.clear()    
        q_avg_series.clear()
        
        #Set global variables from information on UI
        global NGEN
        global POP_SIZE 
        global MUTPB
        global NELT
        NGEN = int(self.tbxGenerations.text())
        # POP_SIZE = int(self.tbxPopulation.text())
        # MUTPB = float(self.tbxMutation.text())
        # NELT = int(self.tbxElite.text())
        global border_check
        border_check = self.cbxBorder.isChecked()
        
        #Loading Croatia map
        self.img = QPixmap(620,600)
        self.img.load('Croatia620.png')
        self.frameWorld.img = self.img
        #Drawing towns
        painter = QPainter(self.img)
        painter.setPen(QPen(Qt.black,  7, Qt.SolidLine))
        painter.setFont(QFont('Arial', 12))
        for i in range(len(gradovi)):
            x, y = GlobToImgCoords(sirine[i], duzine[i])
            painter.drawPoint(x, y)
            painter.drawText(x+5, y+5, gradovi[i])

        painter.end()
        #Redrawing frames
        self.frameWorld.repaint()
        app.processEvents()
        
        ####Initialize deap GA objects####
        
        #Make creator that minimize. If it would be 1.0 instead od -1.0 than it would be maxmize
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        
        #Create an individual (a blueprint for cromosomes) as a list with a specified fitness type
        creator.create("Individual", list, fitness=creator.FitnessMin)
        
        #Create base toolbox for finishing creation of a individual (cromosome)
        self.toolbox = base.Toolbox()
        
        #This is if we want a permutation coding of genes in the cromosome
        self.toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)
        
        #initIterate requires that the generator of genes (such as random.sample) generates an iterable (a list) variable
        self.toolbox.register("individual", tools.initIterate, creator.Individual, self.toolbox.indices)
        
        #Create a population of individuals (cromosomes). The population is then created by toolbox.population(n=300) where 'n' is the number of cromosomes in population
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
        
        #Register evaluation function
        self.toolbox.register("evaluate", evaluateInd)
        
        #Register what genetic operators to use
        self.toolbox.register("mate", tools.cxUniformPartialyMatched, indpb=0.2)#Use uniform recombination for permutation coding
        
        #Permutation coding
        self.toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.2)
        
        self.toolbox.register("select", tools.selTournament, tournsize=3)    #Use tournament selection
        
        ##################################
        
        #Generate initial poplation. Will be a member variable so we can easely pass everything to new thread
        self.pop = self.toolbox.population(n=POP_SIZE)
    
        #Evaluate initial population, we map() the evaluation function to every individual and then assign their respective fitness, map runs evaluate function for each individual in pop
        fitnesses = list(map(self.toolbox.evaluate, self.pop))
        for ind, fit in zip(self.pop, fitnesses):
            ind.fitness.values = fit    #Assign calcualted fitness value to individuals
        
        #Extracting all the fitnesses of all individuals in a population so we can monitor and evovlve the algorithm until it reaches 0 or max number of generation is reached
        self.fits = [ind.fitness.values[0] for ind in self.pop]
        
        #Disable start and enable stop
        self.btnStart.setEnabled(False)
        self.btnStop.setEnabled(True)
        self.gaParams.setEnabled(False)
        self.cbxBorder.setEnabled(False)
        self.cbxNoVis.setEnabled(False)
        
        #Start evolution
        self.evolve(iteration)
        
    
    def btnStop_Click(self):
        global stop_evolution
        stop_evolution = True
        #Disable stop and enable start
        self.btnStop.setEnabled(False)
        self.btnStart.setEnabled(True)
        self.gaParams.setEnabled(True)
        self.cbxBorder.setEnabled(True)
        self.cbxNoVis.setEnabled(True)
    
    #Function for GA evolution
    def evolve(self, iteration):
        global solutions
        global q_min_series
        global q_max_series
        global q_avg_series
        
        # Variable for keeping track of the number of generations
        curr_g = 0
        
        # Begin the evolution till goal is reached or max number of generation is reached
        while min(self.fits) != 0 and curr_g < NGEN:
            #Check if evolution and thread need to stop
            if stop_evolution:
                break #Break the evolution loop
            
            # A new generation
            curr_g = curr_g + 1
            # print("-- Generation %i --" % curr_g)
            
            # Select the next generation individuals
            #Select POP_SIZE - NELT number of individuals. Since recombination is between neigbours, not two naighbours should be the clone of the same individual
            offspring = []
            offspring.append(self.toolbox.select(self.pop, 1)[0])    #add first selected individual
            for i in range(POP_SIZE - NELT - 1):    # -1 because the first seleceted individual is already added
                while True:
                    new_o = self.toolbox.select(self.pop, 1)[0]
                    if new_o != offspring[len(offspring) - 1]:   #if it is different than the last inserted then add to offspring and break
                        offspring.append(new_o)
                        break
            
            # Clone the selected individuals because all of the changes are inplace
            offspring = list(map(self.toolbox.clone, offspring))
            
            # Apply crossover on the selected offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                self.toolbox.mate(child1, child2)    #inplace recombination
                #Invalidate new children fitness values
                del child1.fitness.values
                del child2.fitness.values
    
            #Apply mutation on the offspring
            for mutant in offspring:
                if random.random() < MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
            
            #Add elite individuals #Is clonning needed?
            offspring.extend(list(map(self.toolbox.clone, tools.selBest(self.pop, NELT))))         
                    
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            # print("  Evaluated %i individuals" % len(invalid_ind))
            
            #Replace population with offspring
            self.pop[:] = offspring
            
            # Gather all the fitnesses in one list and print the stats
            self.fits = [ind.fitness.values[0] for ind in self.pop]
            
            length = len(self.pop)
            mean = sum(self.fits) / length
            sum2 = sum(x*x for x in self.fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            q_min_series.append(curr_g, min(self.fits))
            q_max_series.append(curr_g, max(self.fits))
            q_avg_series.append(curr_g, mean)
                      
            # print("  Min %s" % q_min_series.at(q_min_series.count()-1).y())
            # print("  Max %s" % q_max_series.at(q_max_series.count()-1).y())
            # print("  Avg %s" % mean)
            # print("  Std %s" % std)
            
            if self.cbxNoVis.isChecked():
                app.processEvents()
            else:
                self.chart = QChart()
                self.chart.addSeries(q_min_series)
                self.chart.addSeries(q_max_series)
                self.chart.addSeries(q_avg_series)
                self.chart.setTitle("Fitness value over time")
                self.chart.setAnimationOptions(QChart.NoAnimation)
                self.chart.createDefaultAxes()
                self.frameChart.setChart(self.chart)
                
                #Draw queen positions of best individual on a image
                best_ind = tools.selBest(self.pop, 1)[0]
                self.updateWorldFrame(generateWorldImage(best_ind))
                      
        #Printing best individual
        best_ind = tools.selBest(self.pop, 1)[0]
        #print(iteration)
        #print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values[0]))

        ########################################################################################
        # Saving csv
        filename = "csv-ovi/p" + str(POP_SIZE) + "-m" + str(MUTPB) + "-e" + str(NELT) + "-" + str(iteration) + ".csv"
        with open(filename, 'w') as dat:
            for i in range(q_min_series.count()):
                dat.write('%f,%f\n' % (i, q_min_series.at(i).y()))
        
        # Saving map
        self.updateWorldFrame(generateWorldImage(best_ind))
        filename = "karte/p" + str(POP_SIZE) + "-m" + str(MUTPB) + "-e" + str(NELT) + "-" + str(iteration) + ".png"
        self.frameWorld.img.save(filename, "PNG")

        # Remove non avg csvs and maps
        solutions.append(best_ind.fitness.values[0])
        if iteration == 5:
            self.remove_non_avg()
            solutions = []
        ########################################################################################
        
        #Visulaize final solution
        # if self.cbxNoVis.isChecked():
        #     self.chart = QChart()
        #     self.chart.addSeries(q_min_series)
        #     self.chart.addSeries(q_max_series)
        #     self.chart.addSeries(q_avg_series)
        #     self.chart.setTitle("Fitness value over time")
        #     self.chart.setAnimationOptions(QChart.NoAnimation)
        #     self.chart.createDefaultAxes()
        #     self.frameChart.setChart(self.chart)
            
        #     #Draw queen positions of best individual on a image
        #     best_ind = tools.selBest(self.pop, 1)[0]
        #     self.updateWorldFrame(generateWorldImage(best_ind))
        
        #Disable stop and enable start
        self.btnStop.setEnabled(False)
        self.btnStart.setEnabled(True)
        self.gaParams.setEnabled(True)
        self.cbxBorder.setEnabled(True)
        self.cbxNoVis.setEnabled(True)

    def remove_non_avg(self):
        global solutions
        global POP_SIZE
        global MUTPB
        global NELT
        print("Distances: %s" % (solutions), end=",  ")

        _sum = 0
        for solution in solutions:
            _sum += solution
        _avg = _sum/5
        median = (min(solutions, key=lambda x: abs(x - _avg)))
        index = solutions.index(median) + 1
        print("Median = %s" % (median))

        for i in range(1, 6):
            csv_filename = "csv-ovi/p" + str(POP_SIZE) + "-m" + str(MUTPB) + "-e" + str(NELT) + "-" + str(i) + ".csv"
            map_filename = "karte/p" + str(POP_SIZE) + "-m" + str(MUTPB) + "-e" + str(NELT) + "-" + str(i) + ".png"
            
            if i != index:
                # Remove non avg csv
                if os.path.exists(csv_filename):
                    os.remove(csv_filename)

                # Remove non avg map
                if os.path.exists(map_filename):
                    os.remove(map_filename)
                
        
    def updateWorldFrame(self, best_individual_img):
        #new_image = QPixmap(1000,1000)
        self.new_image.fill() #White color is default
        painter = QPainter(self.new_image)
        #First draw the map with towns
        painter.drawPixmap(self.new_image.rect(), self.img)
        #Then draw the best individual
        painter.drawImage(self.new_image.rect(), best_individual_img)
        painter.end()
        #Set new image to the frame
        self.frameWorld.img = self.new_image
        #Redrawing frames
        self.frameWorld.repaint()
        self.frameChart.repaint()
        app.processEvents()
    
    def btnSaveWorld_Click(self):
        filename, _ = QFileDialog.getSaveFileName(None,"Save world as a image","","Image Files (*.png)")
        self.frameWorld.img.save(filename, "PNG")
        print ("World image saved to: ", filename)
    
    def btnSaveChart_CLick(self):
        p = self.frameChart.grab()
        filename, _ = QFileDialog.getSaveFileName(None,"Save series chart as a image","","Image Files (*.png)")
        p.save(filename, "PNG")
        print ("Chart series image saved to: ", filename)
    
    def btnSaveChartSeries_Click(self):
        global q_min_series
        global q_max_series
        global q_avg_series
        filename, _ = QFileDialog.getSaveFileName(None,"Save series to text file","","Text Files (*.txt, *.csv)")
        with open(filename, 'w') as dat:
            for i in range(q_min_series.count()):
                dat.write('%f,%f,%f\n' % (q_min_series.at(i).y(), q_avg_series.at(i).y(), q_max_series.at(i).y()))
        print ("Chart series saved to: ", filename)

    

####################### Kod za provjeru prelaska granice ########################################
class Point: 
	def __init__(self, x, y): 
		self.x = x 
		self.y = y 

def onSegment(p, q, r): 
	if ( (q.x <= max(p.x, r.x)) and (q.x >= min(p.x, r.x)) and
		(q.y <= max(p.y, r.y)) and (q.y >= min(p.y, r.y))): 
		return True
	return False

def orientation(p, q, r): 
	# to find the orientation of an ordered triplet (p,q,r) 
	# function returns the following values: 
	# 0 : Colinear points 
	# 1 : Clockwise points 
	# 2 : Counterclockwise 
	
	val = (float(q.y - p.y) * (r.x - q.x)) - (float(q.x - p.x) * (r.y - q.y)) 
	if (val > 0): 		
		return 1

	elif (val < 0): 
		return 2
        
	else: 	
		return 0

def doIntersect(p1,q1,p2,q2): 
	
	# Find the 4 orientations required for 
	# the general and special cases 
	o1 = orientation(p1, q1, p2) 
	o2 = orientation(p1, q1, q2) 
	o3 = orientation(p2, q2, p1) 
	o4 = orientation(p2, q2, q1) 

	# General case 
	if ((o1 != o2) and (o3 != o4)): 
		return True

	# Special Cases 

	# p1 , q1 and p2 are colinear and p2 lies on segment p1q1 
	if ((o1 == 0) and onSegment(p1, p2, q1)): 
		return True

	# p1 , q1 and q2 are colinear and q2 lies on segment p1q1 
	if ((o2 == 0) and onSegment(p1, q2, q1)): 
		return True

	# p2 , q2 and p1 are colinear and p1 lies on segment p2q2 
	if ((o3 == 0) and onSegment(p2, p1, q2)): 
		return True

	# p2 , q2 and q1 are colinear and q1 lies on segment p2q2 
	if ((o4 == 0) and onSegment(p2, q1, q2)): 
		return True

	# If none of the cases 
	return False


# Calculate paths which cross the border and add them to the matrix
def importBorders():
    global borderCross_paths

    for i in range(IND_SIZE):
        x1, y1 = GlobToImgCoords(sirine[i], duzine[i])
        p1 = Point(x1, y1)

        for j in range(IND_SIZE):
            x2, y2 = GlobToImgCoords(sirine[j], duzine[j])
            q1 = Point(x2, y2)

            for k in range(len(gr_duzine)-1):
                x3, y3 = GlobToImgCoords(gr_sirine[k], gr_duzine[k])
                x4, y4 = GlobToImgCoords(gr_sirine[k+1], gr_duzine[k+1])
                p2 = Point(x3, y3)
                q2 = Point(x4, y4)
                if doIntersect(p1, q1, p2, q2):
                    borderCross_paths[i][j] = 1
                    break
                            
    #print(borderCross_paths)



if __name__ == "__main__":
    importBorders()
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_MainWindow()
    app.setQuitOnLastWindowClosed(True)
    ui.setupUi()
    ui.show()
    sys.exit(app.exec_())
