close all;
clear;
clc;

% Set filenames
path = 'csv-ovi';
f1name = 'p100-m0.1-e5-4.csv';
f2name = 'p100-m0.1-e10-3.csv';
f3name = 'p100-m0.1-e15-5.csv';
f4name = 'p100-m0.1-e20-2.csv';

f1 = fullfile(path, f1name);
f2 = fullfile(path, f2name);
f3 = fullfile(path, f3name);
f4 = fullfile(path, f4name);

% Read .csv files
dataTable1 = readtable(f1);
dataTable2 = readtable(f2);
dataTable3 = readtable(f3);
dataTable4 = readtable(f4);

data1 = table2array(dataTable1);
data2 = table2array(dataTable2);
data3 = table2array(dataTable3);
data4 = table2array(dataTable4);

% generate arrays from .csv files
gens1 = data1(:, 1);
fit1 = data1(:, 2);

gens2 = data2(:, 1);
fit2 = data2(:, 2);

gens3 = data3(:, 1);
fit3 = data3(:, 2);

gens4 = data4(:, 1);
fit4 = data4(:, 2);

% plot graph
figure
plot(gens1, fit1, '-r');
hold on;
plot(gens2, fit2, '--g');
hold on;
plot(gens3, fit3, '-.b');
hold on;
plot(gens4, fit4, ':k');
title('Ovisnost o broju elitnih jedinki');
xlabel('Broj generacija');
ylabel('Vrijednost fitness funkcije');
legend({'5', '10', '15', '20'});
