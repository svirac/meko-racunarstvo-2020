from keras import layers
from keras import models

from keras.datasets import  mnist
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000,28,28,1)
X_train = X_train.astype('float32') / 255
X_test = X_test.reshape(10000,28,28,1)
X_test = X_test.astype('float32') / 255


filter = (5,5)
activation_f = 'relu'

# Model 1
model1 = models.Sequential()
model1.add(layers.Conv2D(64, filter, activation=activation_f, input_shape=(28, 28, 1)))
model1.add(layers.Conv2D(32, filter))
model1.add(layers.Flatten())
model1.add(layers.Dense(10, activation='softmax'))
model1.compile(loss='sparse_categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
model1.summary()
model1.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=5)

print('\n\n')
# Model 2
model2 = models.Sequential()
model2.add(layers.Conv2D(64, filter, activation=activation_f, input_shape=(28, 28, 1)))
model2.add(layers.MaxPooling2D((2,2)))
model2.add(layers.Conv2D(32, filter))
model2.add(layers.MaxPooling2D((2,2)))
model2.add(layers.Flatten())
model2.add(layers.Dense(10, activation='softmax'))
model2.compile(loss='sparse_categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
model2.summary()
model2.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=5)

