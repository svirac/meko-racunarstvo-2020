import numpy as np
from sklearn.datasets import fetch_openml
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


X, y = fetch_openml(name='mnist_784', version=1, return_X_y=True)
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]

ALPHA = 0.00001 
MAX_ITER = 100 
N_PER_LAYER = (100,)
ACTIVATION_F = 'logistic'
SOLVER = 'adam' 

mlp = MLPClassifier(hidden_layer_sizes=N_PER_LAYER, activation=str(ACTIVATION_F),
                            solver=SOLVER, alpha=ALPHA, max_iter=MAX_ITER)
mlp.fit(X_train, y_train)

output = mlp.predict(X_test)

cfm = confusion_matrix(y_test, output)
tp = cfm[1,1]
tn = cfm[0,0]
fp = cfm[0,1]
fn = cfm[1,0]
accuracy = (tp+tn)/(tp+tn+fp+fn)
print("Acc: ", accuracy)

cfm = cfm.astype('float') / cfm.sum(axis=1)[:, np.newaxis]
import seaborn as sns
sns.heatmap(cfm, annot=True, cmap='Blues')
plt.show()